# ESPHOME config files

Flash via `./flash.sh example.flashconfig`.


## esphome yaml config
Place the following placeholders in your yaml config file (passwords are stored in the corresponding .flashconfig file and should not be commited to git)

```yaml
wifi:
  ssid: "WIFI_SSID"
  password: "WIFI_PASSWORD"

  # Enable fallback hotspot (captive portal) in case wifi connection fails
  ap:
    ssid: "HOTSPOT_SSID"
    password: "HOTSPOT_PASSWORD"

ota:
  safe_mode: True
  password: "OTA_PASSWORD"
```

